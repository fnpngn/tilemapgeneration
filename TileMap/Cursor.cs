﻿using CringeFramework.Core;
using CringeFramework.Core.Entities;
using SFML.Graphics;
using SFML.System;

namespace TileMap
{
    public class Cursor : Entity, Drawable
    {
        private Sprite target;
        private Application Application => this.Scene.Application;

        public Cursor()
        {
            target = new Sprite();
        }

        public override void Initialize()
        {
            var tex = new Texture("resources/cross.png");
            target = new Sprite(tex);
            target.Origin = (Vector2f)tex.Size / 2;
        }
        public override void Update()
        {
            target.Position = Application.View.Center;
            target.Scale = Application.ZoomFactor;
            //target.Rotation += 0.5f * Application.DeltaTime;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(this.target);
        }
    }

}
