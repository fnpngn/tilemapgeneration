﻿using CringeFramework.Core;
using SFML.Graphics;
using TileMap;
using TileMap.Generation;

var builder = new ApplicationConfigurationBuilder();
var configuration = builder.WithWindowName("TileMap Application")
                           .WithScene(new TileMapGenerationScene(), "tilemap")
                           .WithInputManager(new InputHandler())                           
                           //.WithIcon("resources/icon.jpeg")
                           .WithIcon(new Image(2, 2, new byte[16] { 255, 0, 255, 255, 255, 255, 0, 255, 0, 255, 255, 255, 0, 0, 255, 255 }))
                           .Build();

var app = Application.Instance.Initialize(configuration);
app.SetActiveScene("tilemap");
app.Run();