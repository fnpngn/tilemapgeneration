﻿using CringeFramework.NoiseTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace TileMap.Generation.Generators.@abstract
{
    public abstract class NoiseMapGenerator
    {
        protected float[] noise;
        protected int noisemapWidth;
        protected float scale;
        protected int octaves;
        protected int seed;
        public int Zoom { get; set; }

        public NoiseMapGenerator()
        {
            scale = 0.005f;
            Zoom = 1;
            seed = Random.Shared.Next();
            octaves = 5;
        }

        public void Initialize()
        {
            Noise2d.Seed(seed);
        }

        public virtual float[] GenerateNoiseMap(int width, int height)
        {
            noise = Noise2d.GeneratePerlinNoiseMap(width, height, octaves, false);
            noisemapWidth = width;
            return noise;
        }
        public virtual float Sample(int x, int y)
        {
            return noise[y * noisemapWidth  + x ];
        }
    }
}
