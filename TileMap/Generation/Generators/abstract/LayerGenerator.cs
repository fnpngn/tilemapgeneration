﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;

namespace TileMap.Generation.Generators.@abstract
{
    public enum LayerType
    {
        Terrain,
        Biome,
        Water
    }
    public abstract class LayerGenerator<T>
    {
        protected NoiseMapGenerator noiseMapGenerator;
        protected T[] layer;
        protected int LayerWidth;
        public int Zoom { get; set; }
        public LayerGenerator(NoiseMapGenerator noiseMapGenerator)
        {
            this.noiseMapGenerator = noiseMapGenerator;
            this.Zoom = 1;
        }

        public abstract T[] GenerateLayer(int sizeX, int sizeY);
        public virtual T Sample(int X, int Y)
        {
            return layer[Y / Zoom * LayerWidth + X / Zoom];
        }
        public virtual T Sample(Vector2i pos)
        {
            return Sample(pos.X, pos.Y);
        }
    }
}
