﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Tiles;
using TileMap.Generation.Tiles.Entities;

namespace TileMap.Generation.Generators.@abstract
{
    public abstract class FeatureGenerator<T> : IChunkVisitor where T : TileEntity
    {
        private Random rand;
        public FeatureGenerator(int seed)
        {
            rand = new Random(seed);
        }

        public abstract void Visit(Chunk chunk);
    }
}
