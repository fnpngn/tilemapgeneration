﻿using CringeFramework.NoiseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;

namespace TileMap.Generation.Generators
{
    public class BiomeNoiseMapGenerator: NoiseMapGenerator
    {
        public BiomeNoiseMapGenerator() {
            this.octaves = 7;
        }
    }
}
