﻿using CringeFramework.NoiseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;

namespace TileMap.Generation.Generators
{
    public class TerrainGenerator : LayerGenerator<float>
    {
        public TerrainGenerator(NoiseMapGenerator noiseMapGenerator) : base(noiseMapGenerator)
        {
        }

        public override float[] GenerateLayer(int sizeX, int sizeY)
        {
            this.layer = new float[sizeX * sizeY];
            this.LayerWidth = sizeX;
            noiseMapGenerator.GenerateNoiseMap(sizeX, sizeY);
            float noise;
            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    noise = noiseMapGenerator.Sample(x, y);
                    layer[y * sizeX + x] = noise;
                }
            }

            return layer;
        }
    }
}
