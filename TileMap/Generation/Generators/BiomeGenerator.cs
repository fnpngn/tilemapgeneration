﻿using CringeFramework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;
using TileMap.Generation.Tiles;
using TileMap.Generation.Tiles.Entities;

namespace TileMap.Generation.Generators
{
    public enum BiomeType : int
    {
        Forest,
        Desert,
        Mountain,
        Ocean,
        River
    }

    public class BiomeBase : IChunkVisitor
    {
        public const int WATER_LEVEL = 40;
        public float Elevation { get; set; }
        public float ElevationDelta { get; set; }
        public virtual void Visit(Chunk chunk)
        {
            var checkList = new List<Tile>();
            var tilemap = Tilemap.Instance;
            foreach (var tile in chunk.GetTilesEnumerable())
            {
                var elevation = tile.Elevation;
                if (elevation < WATER_LEVEL)
                {
                    tile.Type = Tilemap.TileType.Water;
                }
                else
                {
                    tile.Type = Tilemap.TileType.Dirt;
                }
            }
        }
    }

    public class Plains : BiomeBase
    {
        private const float TREE_DENSITY = .7f;
        private const float ROCK_DENSITY = .05f;
        private Random rand;
        public Plains()
        {
            Elevation = 50f;
            ElevationDelta = 7f;
            rand = new Random();
        }
        public override void Visit(Chunk chunk)
        {
            var tiles = chunk.GetTilesEnumerable().ToList();
            for (int i = 0; i < tiles.Count; i++)
            {
                tiles[i].Type = Tilemap.TileType.Grass;
                if (rand.NextSingle() < TREE_DENSITY / tiles.Count)
                {
                    tiles[i].Subscribe(new Tree(tiles[i]));
                }

                if (rand.NextSingle() < ROCK_DENSITY / tiles.Count)
                {
                    tiles[i].Subscribe(new RockFormation(tiles[i]));
                }
            }

        }
    }
    public class Mountains : BiomeBase
    {
        private const float ROCK_DENSITY = .1f;
        private Random rand;
        public Mountains()
        {
            Elevation = 120f;
            ElevationDelta = 30f;
            rand = new Random();
        }
        public override void Visit(Chunk chunk)
        {
            var tiles = chunk.GetTilesEnumerable().ToList();
            for (int i = 0; i < tiles.Count; i++)
            {
                
                if (rand.NextSingle() < ROCK_DENSITY / tiles.Count)
                {
                    tiles[i].Subscribe(new RockFormation(tiles[i]));
                }
                if (tiles[i].Type != Tilemap.TileType.Water)
                {
                    if (tiles[i].Elevation < Elevation - ElevationDelta) 
                        tiles[i].Type = Tilemap.TileType.Grass;
                    else
                        tiles[i].Type = rand.NextSingle() < 0.9f ? Tilemap.TileType.Rock : Tilemap.TileType.Dirt;
                }

            }

        }
    }
    public class Desert : BiomeBase
    {
        private const float ROCK_DENSITY = .01f;
        private Random rand;
        public Desert()
        {
            Elevation = 120f;
            ElevationDelta = 30f;
            rand = new Random();
        }
        public override void Visit(Chunk chunk)
        {
            var tiles = chunk.GetTilesEnumerable().ToList();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (rand.NextSingle() < ROCK_DENSITY / tiles.Count)
                {
                    tiles[i].Subscribe(new RockFormation(tiles[i]));
                }
                if (tiles[i].Type != Tilemap.TileType.Water)
                {
                    tiles[i].Type = rand.NextSingle() < 0.8f ? Tilemap.TileType.Sand : Tilemap.TileType.Dirt;
                }
            }

        }
    }

    public class Ocean : BiomeBase
    {
        private Random rand;
        private const int DEEP_OCEAN_LEVEL = 10;
        public Ocean()
        {
            Elevation = 120f;
            ElevationDelta = 30f;
            rand = new Random();
        }
        public override void Visit(Chunk chunk)
        {
            var tiles = chunk.GetTilesEnumerable().ToList();
            for (int i = 0; i < tiles.Count; i++)
            {
                if (tiles[i].Elevation < DEEP_OCEAN_LEVEL)
                {
                    //
                }
            }

        }
    }
    public class BiomeGenerator : LayerGenerator<BiomeType>
    {
        public BiomeGenerator(NoiseMapGenerator noiseMapGenerator) : base(noiseMapGenerator)
        {
        }

        public override BiomeType[] GenerateLayer(int sizeX, int sizeY)
        {
            this.layer = new BiomeType[sizeX * sizeY];
            this.LayerWidth = sizeX;
            noiseMapGenerator.GenerateNoiseMap(sizeX, sizeY);
            float noise;
            for (int y = 0; y < sizeY; y++)
            {
                for (int x = 0; x < sizeX; x++)
                {
                    noise = noiseMapGenerator.Sample(x, y);
                    if (noise < 0.3f)
                    {
                        layer[y * sizeX + x] = BiomeType.Desert;
                    }
                    else if (noise < 0.5f)
                    {
                        layer[y * sizeX + x] = BiomeType.Forest;
                    }
                    else if (noise < 0.7f)
                    {
                        layer[y * sizeX + x] = BiomeType.Mountain;
                    }
                    else
                    {
                        layer[y * sizeX + x] = BiomeType.Desert;
                    }
                }
            }

            return layer;
        }
    }
}
