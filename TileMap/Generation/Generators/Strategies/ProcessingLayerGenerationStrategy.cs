﻿using CringeFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Tiles;

namespace TileMap.Generation.Generators.Strategies
{
    internal class ProcessingLayerGenerationStrategy : ILayerGenerationStrategy
    {
        private Random rand;
        public ProcessingLayerGenerationStrategy()
        {
            rand = new Random();
        }
        public void GenerateLayer(Tilemap tilemap)
        {
            foreach (var chunk in tilemap.GetChunksEnumerable())
            {
                foreach (var tile in chunk.GetTilesEnumerable())
                {
                    CheckShoreLine(tilemap, tile);
                }
                CheckBlending(tilemap, chunk);
                chunk.SetInitialized(true);
                chunk.SetVisible(true);
            }
        }

        private void CheckShoreLine(Tilemap tilemap, Tile tile)
        {
            if (tile.Elevation == BiomeBase.WATER_LEVEL + 1)
            {
                var pos = tile.Position2i;

                for (int dx = -1; dx <= 1; dx++)
                {
                    for (int dy = -1; dy <= 1; dy++)
                    {
                        pos.X += dx; pos.Y += dy;

                        if (pos.IsWithinBounds(VectorExtensions.Vector2iZero, tilemap.Size - VectorExtensions.Vector2iOne))
                        {
                            if (tilemap.GetTile(pos).Type == Tilemap.TileType.Water)
                            {
                                if (rand.NextSingle() < 0.8f)
                                {
                                    tile.Type = Tilemap.TileType.Sand;
                                }
                            }
                        }

                        pos.X -= dx; pos.Y -= dy;
                    }
                }
            }
        }

        private void CheckBlending(Tilemap tilemap, Chunk chunk)
        {
            Chunk testedChunk;
            if (true)
            {
                var pos = chunk.ChunkCoords;

                for (int dx = -1; dx <= 1; dx++)
                {
                    for (int dy = -1; dy <= 1; dy++)
                    {
                        pos.X += dx; pos.Y += dy;

                        if (pos.IsWithinBounds(VectorExtensions.Vector2iZero, tilemap.ChunksSize - VectorExtensions.Vector2iOne))
                        {
                            testedChunk = tilemap.GetChunk(pos.X, pos.Y);
                            if (testedChunk.BiomeType == chunk.BiomeType) continue;

                            var chunkElevation = chunk.GetAverageElevation();
                            var testedChunkElevation = testedChunk.GetAverageElevation();
                            var c1 = chunk.GetCenterCoords();
                            var c2 = testedChunk.GetCenterCoords();
                            float dist;
                            foreach (var tile in chunk.GetTilesEnumerable())
                            {
                                dist = tile.Position2i.Distance(c2) / Tile.TILE_SIZE - 0.5f;
                                tile.SetElevation((int)MathExtensions.Lerp(chunkElevation, testedChunkElevation, dist));
                                //Console.WriteLine(dist + " " + (int)MathExtensions.Lerp(chunkElevation, testedChunkElevation, dist));
                            }

                            //foreach (var tile in testedChunk.GetTilesEnumerable())
                            //{
                            //    dist = tile.Position2i.Distance(c1) / Tile.TILE_SIZE - 0.5f;
                            //    tile.SetElevation((int)MathExtensions.Lerp(testedChunkElevation, chunkElevation, dist));
                            //}
                        }

                        pos.X -= dx; pos.Y -= dy;
                    }
                }
            }
        }
    }
}
