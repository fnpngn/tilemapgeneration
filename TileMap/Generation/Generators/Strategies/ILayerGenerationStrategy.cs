﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;
using TileMap.Generation.Tiles;

namespace TileMap.Generation.Generators.Strategies
{
    public interface ILayerGenerationStrategy
    {
        void GenerateLayer(Tilemap tilemap);
    }
}
