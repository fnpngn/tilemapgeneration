﻿using CringeFramework.NoiseTools;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;
using TileMap.Generation.Tiles;

namespace TileMap.Generation.Generators.Strategies
{
    public class TerrainGenerationStrategy : ILayerGenerationStrategy
    {
        private TerrainGenerator generator;
        private const int ZOOM = 1;
        private float[] noise;
        public TerrainGenerationStrategy()
        {
            generator = new TerrainGenerator(new TerrainNoiseMapGenerator());
        }
        public void GenerateLayer(Tilemap tilemap)
        {
            generator.Zoom = ZOOM;
            generator.GenerateLayer(tilemap.Size.X / ZOOM, tilemap.Size.Y / ZOOM);
            foreach (var chunk in tilemap.GetChunksEnumerable())
            {
                chunk.FillTiles(tilemap.Size.X);
                foreach (var tile in chunk.GetTilesEnumerable())
                {
                    tile.Type = Tilemap.TileType.Grass;
                    tile.SetElevation(generator.Sample(tile.Position2i));
                }

            }
        }
    }
}
