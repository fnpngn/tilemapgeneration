﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;
using TileMap.Generation.Tiles;

namespace TileMap.Generation.Generators.Strategies
{
    public class BiomeGenerationStrategy : ILayerGenerationStrategy
    {
        private BiomeGenerator generator;
        private const int ZOOM = 1;
        private Plains PlainsBiome;
        private Desert DesertBiome;
        private Mountains MountainBiome;
        private Ocean OceanBiome;
        private BiomeBase BiomeBase;
        public BiomeGenerationStrategy()
        {
            generator = new BiomeGenerator(new BiomeNoiseMapGenerator());
            PlainsBiome = new Plains();
            DesertBiome = new Desert();
            MountainBiome = new Mountains();
            OceanBiome = new Ocean();
            BiomeBase = new BiomeBase();
        }
        public void GenerateLayer(Tilemap tilemap)
        {
            generator.Zoom = ZOOM;
            generator.GenerateLayer(tilemap.Size.X / ZOOM, tilemap.Size.Y / ZOOM);
            foreach (var chunk in tilemap.GetChunksEnumerable())
            {
                var biome = generator.Sample(chunk.Position.X, chunk.Position.Y);
                chunk.BiomeType = biome;
                chunk.Accept(BiomeBase);
                switch (biome)
                {
                    case BiomeType.Forest:
                        chunk.Accept(PlainsBiome);
                        break;
                    case BiomeType.Desert:
                        chunk.Accept(DesertBiome);
                        break;
                    case BiomeType.Mountain:
                        chunk.Accept(MountainBiome);
                        break;
                    case BiomeType.Ocean:
                        chunk.Accept(OceanBiome);
                        break;
                }
            }
        }
    }
}
