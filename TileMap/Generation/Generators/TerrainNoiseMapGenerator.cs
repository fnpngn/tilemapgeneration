﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators.@abstract;

namespace TileMap.Generation.Generators
{
    public class TerrainNoiseMapGenerator: NoiseMapGenerator
    {
        public TerrainNoiseMapGenerator() {
            this.octaves = 7;
        }
    }
}
