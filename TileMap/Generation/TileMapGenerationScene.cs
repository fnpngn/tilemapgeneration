﻿using CringeFramework.Core;
using CringeFramework.Core.Coroutine;
using CringeFramework.NoiseTools;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Tiles;
using static CringeFramework.MathExtensions;

namespace TileMap.Generation
{
    public class TileMapGenerationScene : Scene
    {
        public Vector2i TilemapSize { get; private set; } = new Vector2i(1000, 1000);
        private Tilemap tilemap;
        private Cursor cursor;
        private CoroutineManager coroutines;
        private TilemapGenerator tilemapGenerator;
        private View view => Application.View;

        public TileMapGenerationScene() : base("Tilemap Scene")
        {
            tilemap = new Tilemap(TilemapSize.X, TilemapSize.Y);
            cursor = new Cursor();
            tilemapGenerator = new TilemapGenerator();
        }

        public override void Initialize()
        {
            Application.InputManager.KeyPressed += OnKeyPress;

            this.Add(tilemap);
            this.Add(cursor);
            view.Zoom(50f);
            view.Move((Vector2f)TilemapSize * 10);

            coroutines = Application.Coroutines;
            //coroutines.Start(tilemap.Generate());
            coroutines.Start(GenerateTilemap());
            coroutines.Start(tilemap.DoRandomTicks());
        }

        public IEnumerator GenerateTilemap()
        {
            yield return coroutines.WaitForFrames(2);
            tilemap.Accept(tilemapGenerator);
        }

        public override void Update()
        {
            //Thread.Sleep(750);            
            tilemap.Update();
        }
        private void OnKeyPress(object? sender, KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Escape:
                    Application.Shutdown();
                    break;
                case Keyboard.Key.Space:
                    GetTileInfo();
                    break;
                case Keyboard.Key.H:
                    tilemap.SwitchVisible();
                    break;
                case Keyboard.Key.R:
                    coroutines.Start(GenerateTilemap());
                    break;
                default: break;
            }
        }
        private void GetTileInfo()
        {
            var tile = tilemap.GetTileGuarded(view.Center / Tile.TILE_SIZE, true);
            var chunk = tilemap.GetChunkGlobal(view.Center / Tile.TILE_SIZE);
            Console.WriteLine((tile is null ? "[Tile Inaccessible]" : tile) + chunk?.ToString());
        }
    }
}
