﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Tiles;

namespace TileMap.Generation
{
    public class ChunkSelection : Drawable
    {
        private RectangleShape rect;
        private Vector2f position;
        private bool isActive;

        public bool IsActive => isActive;
        public float OutlineThickness { get { return rect.OutlineThickness; } set { rect.OutlineThickness = value; } }
        public Vector2i ChunkCoords
        {
            get { return ((Vector2i)position) / (Chunk.CHUNK_SIZE * Tile.TILE_SIZE); }
            set
            {
                position = (Vector2f)value * Chunk.CHUNK_SIZE * Tile.TILE_SIZE;
                rect.Position = position;
            }
        }
        public ChunkSelection()
        {
            rect = new RectangleShape(new Vector2f(Chunk.CHUNK_SIZE * Tile.TILE_SIZE, Chunk.CHUNK_SIZE * Tile.TILE_SIZE));
            rect.OutlineThickness = 4;
            rect.OutlineColor = new Color(255, 100, 0, 200);
            rect.FillColor = Color.Transparent;
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (!isActive) return;

            target.Draw(rect);
        }

        public void SetActive(bool state = true)
        {
            isActive = state;
        }

        public void SetOutlineThickness(float thickness)
        {
            rect.OutlineThickness = thickness;
        }
    }
}
