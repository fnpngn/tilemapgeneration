﻿using TileMap.Generation.Tiles;

namespace TileMap.Generation
{
    public interface ITilemapVisitor
    {
        void Visit(Tilemap tilemap);
    }
}