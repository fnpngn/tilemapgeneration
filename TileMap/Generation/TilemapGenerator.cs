﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Tiles;
using TileMap.Generation.Generators.Strategies;

namespace TileMap.Generation
{
    public class TilemapGenerator : ITilemapVisitor
    {
        private List<ILayerGenerationStrategy> strategies = new List<ILayerGenerationStrategy>()
        {
            new TerrainGenerationStrategy(),
            new BiomeGenerationStrategy(),
            new ProcessingLayerGenerationStrategy(),
        };
        public TilemapGenerator() {
            
        }
        public void Visit(Tilemap tilemap)
        {
            foreach (var strategy in strategies)
            {
                strategy.GenerateLayer(tilemap);
            }
        }
    }
}
