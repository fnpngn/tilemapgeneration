﻿namespace TileMap.Generation.Tiles
{
    public interface IChunkVisitor
    {
        public void Visit(Chunk chunk);
    }
}