﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap.Generation.Tiles
{
    public interface ITickObserverNotifier
    {
        void NotifyObservers();
        void Subscribe(ITickObserver observer);
    }
}
