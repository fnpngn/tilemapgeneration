﻿using CringeFramework;
using CringeFramework.Core;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TileMap.Generation.Tiles.Tile;

namespace TileMap.Generation.Tiles.Entities
{
    public class RockFormation : TileEntity
    {
        private readonly static Lazy<Texture> lazyTex = new Lazy<Texture>(() =>
        {
            return new Texture("resources/rocks.png");
        });
        private const short MAX_STAGE = 20;
        private const short MIN_STAGE = 5;
        private short stage;
        public RockFormation(Tile tile) : base(tile)
        {
            this.sprite = new Sprite(lazyTex.Value);
            stage = MAX_STAGE;
            sprite.Position = tile.GlobalPosition;
            sprite.Scale = VectorExtensions.FromValue((float)stage / MAX_STAGE * TILE_SIZE);
        }
        public override void Tick()
        {
            if (stage > MIN_STAGE) stage--;
            sprite.Scale = VectorExtensions.FromValue((float)stage / MAX_STAGE * TILE_SIZE);
        }
    }
}
