﻿using CringeFramework;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TileMap.Generation.Tiles.Tile;
namespace TileMap.Generation.Tiles.Entities
{
    public class Tree : TileEntity
    {
        private readonly static Lazy<Texture> lazyTex = new Lazy<Texture>(() =>
        {
            return new Texture("resources/tmree.png");
        });
        private const short MAX_GROWTH = 20;
        private const short MIN_GROWTH = 5;
        private const float DECAY_CHANCE = 0.1f;
        private short growth;
        public Tree(Tile tile) : base(tile)
        {
            this.sprite = new Sprite(lazyTex.Value);
            growth = MIN_GROWTH;
            sprite.Position = tile.GlobalPosition;
            sprite.Scale = VectorExtensions.FromValue((float)growth / MAX_GROWTH);
        }
        public override void Tick()
        {
            if (growth < MAX_GROWTH)
            {
                growth++;
                sprite.Scale = VectorExtensions.FromValue((float)growth / MAX_GROWTH * TILE_SIZE);
            }
            else if (Random.Shared.NextSingle() < DECAY_CHANCE)
            {
                this.Destroy();
            }
        }
    }
}
