﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TileMap.Generation.Tiles.Entities
{
    public abstract class TileEntity : ITickObserver, Drawable
    {
        public Tile Tile { get; protected set; }
        protected bool toRender;
        protected Sprite sprite;

        public TileEntity(Tile tile)
        {
            Tile = tile;
            toRender = true;
            Tilemap.Instance.Scene.AddRenderElement(this);
        }

        public abstract void Tick();

        public virtual void Draw(RenderTarget target, RenderStates states)
        {
            if (!toRender) return;
            sprite.Draw(target, states);
        }

        public virtual void Destroy()
        {
            this.Tile.Unsubscribe(this);
            Tilemap.Instance.Scene.RemoveRenderElement(this);
        }
    }
}
