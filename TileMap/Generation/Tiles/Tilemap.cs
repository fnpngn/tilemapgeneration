﻿using CringeFramework;
using CringeFramework.Core;
using CringeFramework.Core.Coroutine;
using CringeFramework.Core.Entities;
using CringeFramework.NoiseTools;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using TileMap.Generation.Generators;
using TileMap.Generation.Generators.@abstract;
using static CringeFramework.VectorExtensions;
using static TileMap.Generation.Tiles.Chunk;

namespace TileMap.Generation.Tiles
{

    public class Tilemap : Entity, Drawable
    {
        private Chunk[] chunks;
        private Vector2i chunksSize;
        private ChunkSelection selection;
        private Vertex[] vertices;
        private bool isGenerating;
        private NoiseMapGenerator noise;
        private bool toRender;

        public static Tilemap Instance;

        public Vector2i Size { get; private set; }
        public Vector2i ChunksSize => chunksSize;

        public Tilemap(int width, int height)
        {
            Instance = this;
            isGenerating = false;
            width = (width + CHUNK_SIZE) / CHUNK_SIZE * CHUNK_SIZE;
            height = (height + CHUNK_SIZE) / CHUNK_SIZE * CHUNK_SIZE;
            int chunkWidth = (width + CHUNK_SIZE - 1) / CHUNK_SIZE;
            int chunkHeight = (height + CHUNK_SIZE - 1) / CHUNK_SIZE;
            chunksSize = new Vector2i(chunkWidth, chunkHeight);

            int vertexArraySize = Math.Max(width * height, CHUNK_SIZE * CHUNK_SIZE) * 4;
            chunks = new Chunk[chunkHeight * chunkWidth];

            Size = new Vector2i(width, height);
            vertices = new Vertex[vertexArraySize];
            selection = new ChunkSelection();

            for (int y = 0; y < chunkHeight; y++)
            {
                for (int x = 0; x < chunkWidth; x++)
                {
                    chunks[y * chunkWidth + x] = new Chunk(x, y);
                }
            }
            noise = new TerrainNoiseMapGenerator();
            toRender = true;
        }

        public override void Update()
        {
            selection.SetOutlineThickness(2f * Application.Instance.ZoomFactor.X);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {

            //for (int y = 0; y < chunksSize.Y; y++)
            //{
            //    for (int x = 0; x < chunksSize.X; x++)
            //    {
            //        chunks[y * chunksSize.X +  x].Draw(target, ref vertices, y * chunksSize.X + x);
            //    }
            //}
            if (!toRender)
            {
                return;
            }

            for (int index = 0; index < chunks.Length; index++)
            {
                chunks[index].DrawTo(target, ref vertices, index);
            }
            target.Draw(vertices, PrimitiveType.Quads);
            target.Draw(selection);
        }


        public IEnumerator DoRandomTicks()
        {
            var coroutines = Application.Instance.Coroutines;
            Chunk[] tickChunks;
            var rand = new Random(Guid.NewGuid().GetHashCode());
            int N = 0;

            yield return coroutines.WaitForFrames(1);
            while (true)
            {
                N = rand.Next(Math.Min(100, chunks.Length));
                tickChunks = chunks.PickNRandom(N);
                for (int i = 0; i < N; i++)
                {
                    tickChunks[i].RandomTick();
                }
                yield return coroutines.WaitForSeconds(1f);
            }
        }

        //public IEnumerator Generate()
        //{
        //    if (isGenerating)
        //    {
        //        yield break;
        //    }
        //    isGenerating = true;
        //    //Task.Run(GenerateChunksParallel);
        //    yield break;
        //    ////float[] noiseMap = Flatten(Noise2d.GenerateNoiseMap(Size.Y, Size.X, 5f, 4));
        //    //float[] noiseMap = Noise2d.GeneratePerlinNoiseMap(Size.X, Size.Y, 2);
        //    //int perFrame = 5;
        //    //for (int index = 0; index < chunks.Length; index++)
        //    //{
        //    //    chunks[index].Generate(noiseMap, Size.X);
        //    //    if (index % perFrame == 0)
        //    //    {
        //    //        yield return 0;
        //    //    }
        //    //}
        //}

        //private void GenerateChunksParallel()
        //{
        //    noise.Initialize();
        //    //float[] noiseMap = Noise2d.GeneratePerlinNoiseMap(Size.X, Size.Y, 7);
        //    float[] noiseMap = noise.GenerateNoiseMap(Size.X, Size.Y);

        //    Console.WriteLine("Chunks to generate: " + chunks.Length);
        //    Stopwatch st = Stopwatch.StartNew();
        //    int tileMapWidth = Size.X;
        //    Parallel.For(0, chunks.Length, index =>
        //    {
        //        chunks[index].Generate(noiseMap, tileMapWidth);
        //    });
        //    st.Stop();
        //    Console.WriteLine("Chunks generated in " + st.Elapsed.TotalSeconds + "s");
        //    isGenerating = false;
        //}

        public Tile? GetTileGuarded(Vector2f pos, bool snap = false)
        {
            var position = (Vector2i)pos;
            if (!position.IsWithinBounds(Vector2iZero, Size - Vector2iOne))
            {
                if (snap)
                {
                    position = position.Clamp(Vector2iZero, Size - Vector2iOne);
                }
                else
                {
                    throw new ArgumentOutOfRangeException(nameof(pos));
                }
            }
            var chunk = chunks[position.Y / CHUNK_SIZE * chunksSize.X + position.X / CHUNK_SIZE];
            if (chunk.IsAccessible)
            {
                selection.ChunkCoords = chunk.ChunkCoords;
                selection.SetActive();
                return chunk.GetTile(position.X % CHUNK_SIZE, position.Y % CHUNK_SIZE);
            }
            selection.SetActive(false);
            return null;
        }
        public Chunk? GetChunkGlobal(Vector2f pos)
        {
            var position = (Vector2i)pos;
            if (!position.IsWithinBounds(Vector2iZero, Size - Vector2iOne))
            {
                position = position.Clamp(Vector2iZero, Size - Vector2iOne);
            }
            var chunk = chunks[position.Y / CHUNK_SIZE * chunksSize.X + position.X / CHUNK_SIZE];
            if (chunk.IsAccessible)
            {
                return chunk;
            }
            return null;
        }
        public Tile GetTile(Vector2f position)
        {
            var p = (Vector2i)position;
            return GetTile(p.X, p.Y);
        }
        public Tile GetTile(Vector2i position)
        {
            return GetTile(position.X, position.Y);
        }
        public Tile GetTile(int x, int y)
        {
            var chunk = chunks[y / CHUNK_SIZE * chunksSize.X + x / CHUNK_SIZE];
            return chunk.GetTile(x % CHUNK_SIZE, y % CHUNK_SIZE);
        }
        public Chunk GetChunk(int x, int y)
        {
            return chunks[y * chunksSize.X + x];
        }

        public IEnumerable<Chunk> GetChunksEnumerable()
        {
            for (int i = 0; i < chunks.Length; i++)
            {
                yield return chunks[i];
            }
        }

        public void Accept(ITilemapVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void SwitchVisible()
        {
            this.toRender = !this.toRender;
        }

        public enum TileType : int
        {
            Water,
            Grass,
            Dirt,
            Rock,
            Sand
        }
    }

}
