﻿using CringeFramework;
using CringeFramework.NoiseTools;
using SFML.Graphics;
using SFML.System;
using System.Reflection.Metadata.Ecma335;
using TileMap.Generation.Generators;

namespace TileMap.Generation.Tiles
{
    public class Chunk : ITickObserverNotifier
    {
        public const int CHUNK_SIZE = 16;
        //private const int VERTEX_CONVERSION_FACTOR = CHUNK_SIZE * CHUNK_SIZE * 4;

        private bool isInitialized;
        private bool toRender;
        private Tile[] tiles;
        private Vertex[] vertexArray;
        private Vector2i coords;
        public BiomeType BiomeType { get; internal set; }

        public bool IsAccessible => isInitialized;
        public Vector2i ChunkCoords => coords;
        public Vector2i Position => coords * CHUNK_SIZE;
        public Chunk(int chunkX, int chunkY)
        {
            tiles = new Tile[CHUNK_SIZE * CHUNK_SIZE];
            coords = new Vector2i(chunkX, chunkY);
            vertexArray = new Vertex[CHUNK_SIZE * CHUNK_SIZE * 4];
            isInitialized = false;
            toRender = true;
        }

        public void FillTiles(int mapWidth)
        {
            //var random = Random.Shared;
            Vector2i pos = new Vector2i
            {
                X = coords.X * CHUNK_SIZE,
                Y = coords.Y * CHUNK_SIZE
            };
            for (int i = 0; i < CHUNK_SIZE; i++)
            {
                for (int j = 0; j < CHUNK_SIZE; j++)
                {
                    //Console.WriteLine(noiseMap[(int)pos.Y, (int)pos.X]);
                    tiles[i * CHUNK_SIZE + j] = new Tile(pos);
                    pos.X++;
                }
                pos.Y++;
                pos.X = coords.X * CHUNK_SIZE;
            }

            isInitialized = true;
            toRender = true;
        }

        public void RandomTick()
        {
            NotifyObservers();
            toRender = true;
        }
        public void NotifyObservers()
        {
            if (!isInitialized) return;
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].Tick();
            }
        }

        public void Subscribe(ITickObserver observer)
        {
            throw new NotImplementedException();
        }
        public void DrawTo(RenderTarget target, ref Vertex[] tilemapVertexBuffer, int startIndex)
        {
            if (!isInitialized || !toRender)
            {
                return;
            }
            startIndex *= CHUNK_SIZE * CHUNK_SIZE * 4;
            for (int y = 0; y < CHUNK_SIZE; y++)
            {
                for (int x = 0; x < CHUNK_SIZE; x++)
                {
                    tiles[y * CHUNK_SIZE + x].Draw(vertexArray, y, x, CHUNK_SIZE);
                }
            }
            //Console.WriteLine($"For chunk [{coords.X},{coords.Y}] drawn buffer from {startIndex} to {startIndex + vertexArray.Length} ({vertexArray.Length})");
            vertexArray.CopyTo(tilemapVertexBuffer, startIndex);
            toRender = false;
        }

        public Tile GetTile(int x, int y)
        {
            return tiles[y * CHUNK_SIZE + x];
        }

        public Vector2i GetCenterCoords()
        {
            return this.coords * CHUNK_SIZE + new Vector2i(CHUNK_SIZE / 2, CHUNK_SIZE / 2);
        }

        public IEnumerable<Tile> GetTilesEnumerable()
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                yield return tiles[i];
            }
        }
        public void Accept(IChunkVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void SetVisible(bool state)
        {
            toRender = state;
        }

        public void SetInitialized(bool state)
        {
            isInitialized = state;
        }

        public override string ToString()
        {
            return $"Chunk {this.ChunkCoords.ToTinyString()} b:{this.BiomeType}";
        }
    }

    public static class ChunkExtensions
    {
        public static float GetAverageElevation(this Chunk c)
        {
            float elevation = 0;
            int count = 0;
            foreach (var tile in c.GetTilesEnumerable())
            {
                elevation += tile.Elevation;
                count++;
            }
            return elevation / count;
        }
    }


}
