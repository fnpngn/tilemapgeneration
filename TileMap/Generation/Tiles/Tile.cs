﻿using CringeFramework;
using SFML.Graphics;
using SFML.System;
using System.Runtime.CompilerServices;
using TileMap.Generation.Tiles.Entities;
using static TileMap.Generation.Tiles.Tilemap;
using static TileMap.Generation.Tiles.Tilemap.TileType;

namespace TileMap.Generation.Tiles
{
    public class Tile : ITickObserver, ITickObserverNotifier
    {
        public const int TILE_SIZE = 20;
        private float elevation;

        private List<ITickObserver> tileEntities;

        public int Elevation => (int)(elevation * 255f);
        public TileType Type { get; set; }
        public Vector2f Position { get; private set; }
        public Vector2f GlobalPosition => this.Position * TILE_SIZE;
        public Vector2i Position2i => (Vector2i)Position;

        public Tile(Vector2i position)
        {
            this.elevation = 0;
            this.Position = (Vector2f)position;
            this.tileEntities = new List<ITickObserver>();
        }
        public Tile(TileType type, Vector2i position, float elevation)
        {
            this.Type = type;
            this.Position = (Vector2f)position;
            //this.size = size;
            this.elevation = elevation;
            this.tileEntities = new List<ITickObserver>();
        }

        public void Draw(Vertex[] vertexArray, int row, int col, int cols)
        {
            int index = (row * cols + col) * 4;
            Vector2f pos = Position * TILE_SIZE;
            var color = TileColors[Type].Multiply(elevation * .5f + .5f);
            vertexArray[index++] = new Vertex(pos, color);
            pos.Y += TILE_SIZE;
            vertexArray[index++] = new Vertex(pos, color);
            pos.X += TILE_SIZE;
            vertexArray[index++] = new Vertex(pos, color);
            pos.Y -= TILE_SIZE;
            vertexArray[index] = new Vertex(pos, color);
        }

        public override string ToString()
        {
            return $"Tile [{Position.X},{Position.Y}] ({Enum.GetName(typeof(TileType), Type)}, e:{this.Elevation})";
        }

        public void Tick()
        {
            NotifyObservers();
        }

        public void NotifyObservers()
        {
            foreach (var tileEntity in this.tileEntities)
            {
                tileEntity.Tick();
            }
        }

        public void Subscribe(ITickObserver observer)
        {
            tileEntities.Add(observer);
        }

        public void Unsubscribe(ITickObserver observer)
        {
            tileEntities.Remove(observer);
        }
        public void SetElevation(float elevation)
        {
            this.elevation = elevation;
        }
        public void SetElevation(int elevation)
        {
            this.elevation = elevation / 255f;
        }
        public static readonly Dictionary<TileType, Color> TileColors = new()
        {
            { Dirt , new Color(2035632383u) },
            { Grass, Color.Green},
            { Water, Color.Blue},
            { Rock, new Color(1431655935u)},
            { Sand, new Color(3266478335u)}
        };
    }
}