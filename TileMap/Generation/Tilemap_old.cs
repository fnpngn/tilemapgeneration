﻿using SFML.Graphics;
using SFML.System;
using System.Diagnostics;
using TileMap.Generation.Tiles;

namespace TileMap.Generation
{
    // Define the tilemap class that represents a grid of tiles
    public class Tilemap_old : Drawable
    {
        private int rows;
        private int cols;

        // The size of each tile in pixels
        private int tileSize;

        private int chunkSize;

        // The array of tiles
        private Tile[,] tiles;

        private Vertex[] tileVertexArray;
        private bool shouldReRender;

        private int counter = 0;
        private object lockObj = new object();

        // The constructor that takes the number of rows, columns, and tile size
        public Tilemap_old(int rows, int cols, int tileSize)
        {
            this.rows = rows;
            this.cols = cols;
            this.tileSize = tileSize;
            chunkSize = 50;
            tileVertexArray = new Vertex[rows * cols * 4];
            shouldReRender = true;

            // Initialize the tiles array
            tiles = new Tile[rows, cols];


        }

        public void Generate()
        {
            Stopwatch st = new Stopwatch();
            Random random = new Random();
            Console.WriteLine("Generating tilemap");
            counter = 0;
            st.Start();
            Parallel.For(0, rows, i =>
            {
                if (Interlocked.Increment(ref counter) % 100 == 0)
                {
                    Console.WriteLine((float)counter / rows * 100f + "% generation");
                }

                for (int j = 0; j < cols; j++)
                {
                    // Create a new tile with a random color and position
                    //Color color = new Color((byte)random.Next(256), (byte)random.Next(256), (byte)random.Next(256));
                    //Vector2f position = new Vector2f(j * tileSize, i * tileSize);
                    //tiles[i, j] = new Tile(color, position);
                }
            });
            st.Stop();
            Console.WriteLine("Generation time: " + st.ElapsedMilliseconds + "ms");
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (shouldReRender)
            {
                Console.WriteLine("Composing vertex array");
                counter = 0;
                Parallel.For(0, rows, i =>
                {
                    if (Interlocked.Increment(ref counter) % 100 == 0)
                    {
                        Console.WriteLine((float)counter / rows * 100 + "% render composition");
                    }

                    for (int j = 0; j < cols; j++)
                    {
                        tiles[i, j].Draw(tileVertexArray, i, j, cols);
                    }
                });
                Console.WriteLine("Render Composition Complete");

                shouldReRender = false;
            }
            target.Draw(tileVertexArray, PrimitiveType.Quads);
        }

        internal class TileChunk : Drawable
        {

            private Tilemap_old tilemap;
            private bool isReady;
            private Vector2i coords;

            public Vector2i ChunkCoords => coords;

            public TileChunk(Vector2i chunkCoords, Tilemap_old parentTilemap)
            {
                isReady = false;
                coords = chunkCoords;
                //                tileVertexArray = vArray;
                tilemap = parentTilemap;
            }


            public void Draw(RenderTarget target, RenderStates states)
            {
                if (!isReady) return;

                var from = coords * tilemap.chunkSize;
                for (int i = from.X; i < from.X + tilemap.chunkSize; i++)
                {
                    for (int j = from.Y; j < from.Y + tilemap.chunkSize; j++)
                    {
                        tilemap.tiles[i, j].Draw(tilemap.tileVertexArray, i, j, tilemap.cols);
                    }
                }
            }
        }
    }
}