# TODO

#### Concurrency
[?] Employ async/await for concurrent chunk generation instead of coroutines.    
[] Thread Pool    

#### Generation   
[]    
[] River generation?
* A\*,     
<br></br>

[] Biome generation     
[] Biome temperature    
[] Test perlin noise with harmonies    
[] Feature generation (decorators?)     


#### Cringe    
[?] dt, framerate     
[x] consistent aspect ratio (+zoom factor)     
[] job wrapper    
[] events, lifecycle      
[40%] extend component system, proper component management, entity class     
[x] input events    
[] Test derived Component cloning w & w/o copy constructor [Activator]
