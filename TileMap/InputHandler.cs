﻿using CringeFramework;
using CringeFramework.Core;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace TileMap
{
    public class InputHandler : InputManager
    {
        private View view { get { return Application.Instance.View; } }

        private bool mousePressed;
        private Vector2i prevMousePos;
        public InputHandler()
        {
            mousePressed = false;
            prevMousePos = new Vector2i(0, 0);
        }

        public override void Initialize()
        {
            this.MouseWheelScrolled += OnMouseWheelScrolled;
        }

        private void OnMouseWheelScrolled(object? sender, MouseWheelScrollEventArgs e)
        {
            // Get the mouse wheel scroll amount
            float zoom = e.Delta;
            // Zoom the window view by a factor of 0.9 or 1.1 depending on the scroll direction
            if (zoom > 0)
            {
                view.Zoom(0.9f);
            }
            else
            {
                view.Zoom(1.1f);
            }
        }

        public override void Update()
        {
            if (Mouse.IsButtonPressed(Mouse.Button.Left))
            {
                // If this is the first time the mouse is pressed, store the mouse position
                if (!mousePressed)
                {
                    mousePressed = true;
                    prevMousePos = Mouse.GetPosition(window);
                }
                else
                {
                    // If the mouse is already pressed, get the current mouse position
                    Vector2i currMousePos = Mouse.GetPosition(window);

                    // Calculate the mouse movement
                    Vector2i delta = currMousePos - prevMousePos;
                    Vector2f zoomFactor = new Vector2f(view.Size.X / window.Size.X, view.Size.Y / window.Size.Y);

                    // Move the window view by the negative of the mouse movement
                    view.Move(new Vector2f(-delta.X * zoomFactor.X, -delta.Y * zoomFactor.Y));

                    // Update the previous mouse position
                    prevMousePos = currMousePos;
                }
            }
            else
            {
                // If the mouse is not pressed, reset the mouse pressed flag
                mousePressed = false;
            }
        }
    }
}
