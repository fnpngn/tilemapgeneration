﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework
{
    public interface IRenderer
    {
        void ResizeWindow(uint width, uint height);
        void ResizeWindow(Vector2u size);
        void ResizeRenderView(uint width, uint height);
        void Render(Drawable element);
    }
}
