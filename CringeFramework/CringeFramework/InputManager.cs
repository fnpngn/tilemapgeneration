﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;

namespace CringeFramework
{
    // Define the input manager class that handles user input
    public class InputManager
    {

        public event EventHandler<MouseWheelScrollEventArgs> MouseWheelScrolled = delegate { };
        public event EventHandler<KeyEventArgs> KeyPressed = delegate { };
        public event EventHandler<KeyEventArgs> KeyReleased = delegate { };
        public event EventHandler GainedFocus = delegate { };
        public event EventHandler LostFocus = delegate { };

        // A reference to the main window
        protected RenderWindow window;
        protected bool ignoreOnLostFocus;

        internal void InitializeInternal(RenderWindow window)
        {
            this.window = window;
            ignoreOnLostFocus = true;
            window.MouseWheelScrolled += (o, e) => MouseWheelScrolled(o, e);
            window.KeyPressed += (o, e) => KeyPressed(o, e);
            window.KeyReleased += (o, e) => KeyReleased(o, e);
            window.GainedFocus += (o, e) => GainedFocus(o, e);
            window.LostFocus += (o, e) => LostFocus(o, e);
            Initialize();
        }
        public void UpdateInternal()
        {
            if (!window.IsOpen) return;
            window.DispatchEvents();

            if (ignoreOnLostFocus && !window.HasFocus()) return;

            Update();
        }

        public virtual void Initialize() { }
        public virtual void Update() { }
    }
}