﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework
{
    public static class ArrayExtensions
    {
        /// <summary>
        /// Flatten a two-dimensional array
        /// </summary>
        /// <typeparam name="T">Array type</typeparam>
        /// <param name="array">Array to flatten</param>
        /// <returns>Single-dimensional shallow copy</returns>
        public static T[] Flatten<T>(this T[,] array)
        {
            var result = new T[array.GetLength(0) * array.GetLength(1)];
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    result[i * array.GetLength(1) + j] = array[i, j];
                }
            }
            return result;
        }

        /// <summary>
        /// Access flat 2d array element based on <paramref name="matrixWidth"/>
        /// </summary>
        /// <typeparam name="T">Array type</typeparam>
        /// <param name="array">Flat 2d array</param>
        /// <param name="matrixWidth">Width of the flattened matrix</param>
        /// <returns>Element at <paramref name="x"/> <paramref name="y"/></returns>
        public static T GetByXY<T>(this T[] array, int x, int y, int matrixWidth)
        {
            return array[y * matrixWidth + y];
        }        

        public static List<T> PickNRandom<T>(this List<T> list, int N)
        {
            if (N > list.Count)
            {
                throw new ArgumentException("N bigger than list size");
            }
            var result = new List<T>(N);
            var count = list.Count;
            var rand = new Random(Guid.NewGuid().GetHashCode());
            float selectionProbability;

            for (int i = 0; i < list.Count && N > 0; i++)
            {
                selectionProbability = (float)N / count;

                if (rand.NextSingle() <= selectionProbability)
                {
                    result.Add(list[i]);
                    N--;
                }
                count--;
            }
            return result;
        }
        public static T[] PickNRandom<T>(this T[] array, int N)
        {
            if (N > array.Length)
            {
                throw new ArgumentException("N bigger than array size");
            }
            var result = new T[N];
            var count = array.Length;
            var rand = new Random(Guid.NewGuid().GetHashCode());
            float selectionProbability;

            for (int i = 0; i < array.Length && N > 0; i++)
            {
                selectionProbability = (float)N / count;
                if (rand.NextSingle() <= selectionProbability)
                {
                    result[N - 1] = array[i];
                    N--;
                }
                count--;
            }
            return result;
        }

        /// <summary>
        /// Resize image contents
        /// </summary>
        /// <remarks>
        /// Does not apply any interpolation algorithms
        /// </remarks>
        /// <param name="image">Image to resize</param>
        /// <param name="width">Target <see cref="Image"/> width</param>
        /// <param name="height">Target <see cref="Image"/> height</param>
        /// <returns>Returns a resized <see cref="Image"/> copy of the current <see cref="Image"/></returns>
        public static Image ResizeEager(this Image image, uint width, uint height)
        {
            var size = image.Size;
            var pixels = image.Pixels;
            var newPixels = new byte[width * height * 4];

            uint origX, origY, index = 0, origIndex;
            for (uint y = 0u; y < height; ++y)
            {
                for (uint x = 0u; x < width; ++x)
                {
                    origX = (uint)((double)x / width * size.X);
                    origY = (uint)((double)y / height * size.Y);
                    origIndex = (origY * size.X + origX) * 4;
                    //index = (y * width + x) * 4u;

                    newPixels[index++] = pixels[origIndex++];
                    newPixels[index++] = pixels[origIndex++];
                    newPixels[index++] = pixels[origIndex++];
                    newPixels[index++] = pixels[origIndex++];
                }
            }

            return new Image(width, height, newPixels);
        }
    }
}
