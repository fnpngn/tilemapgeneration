﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework
{
    public static class VectorExtensions
    {
        public static readonly Vector2i Vector2iZero = new Vector2i(0, 0);
        public static readonly Vector2u Vector2uZero = new Vector2u(0u, 0u);
        public static readonly Vector2f Vector2fZero = new Vector2f(0f, 0f);
        public static readonly Vector2i Vector2iOne = new Vector2i(1, 1);
        public static readonly Vector2u Vector2uOne = new Vector2u(1u, 1u);
        public static readonly Vector2f Vector2fOne = new Vector2f(1f, 1f);
        public static float Distance(this Vector2f v1, Vector2f v2)
        {
            return MathF.Sqrt((v2.X - v1.X) * (v2.X - v1.X) + (v2.Y - v1.Y) * (v2.Y - v1.Y));
        }
        public static float Distance(this Vector2i v1, Vector2i v2)
        {
            return MathF.Sqrt((v2.X - v1.X) * (v2.X - v1.X) + (v2.Y - v1.Y) * (v2.Y - v1.Y));
        }

        public static Vector2f FromValue(float value)
        {
            return new Vector2f(value, value);
        }
        public static Vector2f InitFrom(this Vector2f v, Vector2i from)
        {
            v.X = from.X;
            v.Y = from.Y;
            return v;
        }

        public static string ToTinyString(this Vector2f v)
        {
            return "(" + v.X + "," + v.Y + ")";
        }
        public static string ToTinyString(this Vector2i v)
        {
            return "(" + v.X + "," + v.Y + ")";
        }
        public static string ToTinyString(this Vector2u v)
        {
            return "(" + v.X + "," + v.Y + ")";
        }

        public static bool IsWithinBounds(this Vector2f v, Vector2f from, Vector2f to)
        {
            return v.X >= from.X && v.Y >= from.Y && v.X <= to.X && v.Y <= to.Y;
        }
        public static bool IsWithinBounds(this Vector2i v, Vector2i from, Vector2i to)
        {
            return v.X >= from.X && v.Y >= from.Y && v.X <= to.X && v.Y <= to.Y;
        }

        public static Vector2i Clamp(this Vector2i v, Vector2i lower, Vector2i upper)
        {
            return new Vector2i(Math.Clamp(v.X, lower.X, upper.X), Math.Clamp(v.Y, lower.Y, upper.Y));
        }

        public static Vector2f Lerp(this Vector2i edge0, Vector2i edge1, float x)
        {
            return new Vector2f(MathExtensions.Lerp(edge0.X, edge1.X, x), MathExtensions.Lerp(edge0.Y, edge1.Y, x));
        }
        public static Vector2f Lerp(this Vector2f edge0, Vector2f edge1, float x)
        {
            return new Vector2f(MathExtensions.Lerp(edge0.X, edge1.X, x), MathExtensions.Lerp(edge0.Y, edge1.Y, x));
        }
    }
}
