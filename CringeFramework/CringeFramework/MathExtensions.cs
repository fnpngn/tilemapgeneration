﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework
{
    public static class MathExtensions
    {
        public static float Smoothstep(float edge0, float edge1, float x)
        {
            x = Clamp((x - edge0) / (edge1 - edge0));

            return x * x * (3.0f - 2.0f * x);
        }
        public static Vector2f Smoothstep(Vector2f edge0, Vector2f edge1, float x, float smoothness = 0.5f)
        {
            return new Vector2f()
            {
                X = Smoothstep(edge0.X, edge1.X, x),
                Y = Smoothstep(edge0.Y, edge1.Y, x)
            };
        }

        public static float Clamp(float x, float lower = 0.0f, float upper = 1.0f)
        {
            return Math.Clamp(x, lower, upper);
        }

        public static float ScaleClamp(float value, float min, float max, float min2, float max2)
        {
            value = min2 + (value - min) / (max - min) * (max2 - min2);
            if (max2 > min2)
            {
                value = value < max2 ? value : max2;
                return value > min2 ? value : min2;
            }
            value = value < min2 ? value : min2;
            return value > max2 ? value : max2;
        }

        public static Color LerpColor(Color edge0, Color edge1, float x)
        {
            if (x <= 0) return new Color(edge0);
            if (x >= 1) return new Color(edge1);

            var c = new Color(edge0)
            {
                R = (byte)(edge0.R + (edge1.R - edge0.R) * x),
                G = (byte)(edge0.G + (edge1.G - edge0.G) * x),
                B = (byte)(edge0.B + (edge1.B - edge0.B) * x),
                A = (byte)(edge0.A + (edge1.A - edge0.A) * x)
            };
            return c;
        }
        public static Color Multiply(this Color c, float d)
        {
            c.R = (byte)(c.R * d);
            c.G = (byte)(c.G * d);
            c.B = (byte)(c.B * d);
            return c;
        }

        public static float Lerp(float edge0, float edge1, float x)
        {
            return edge0 * (1 - x) + edge1 * x;
        }
        public static float Lerp(int edge0, int edge1, float x)
        {
            return edge0 * (1 - x) + edge1 * x;
        }
    }
}
