﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;

namespace CringeFramework.NoiseTools
{
    public static class Noise2d
    {
        private static Random _random = new Random();
        private static int[] _permutation;

        private static Vector2[] _gradients;

        static Noise2d()
        {
            CalculatePermutation(out _permutation);
            CalculateGradients(out _gradients);
        }

        private static void CalculatePermutation(out int[] p)
        {
            p = Enumerable.Range(0, 256).ToArray();

            /// shuffle
            for (var i = 0; i < p.Length; i++)
            {
                var source = _random.Next(p.Length);

                var t = p[i];
                p[i] = p[source];
                p[source] = t;
            }
        }

        public static void Seed(int seed)
        {
            _random = new Random(seed);
            CalculatePermutation(out _permutation);
            CalculateGradients(out _gradients);
        }

        public static void Reseed()
        {
            CalculatePermutation(out _permutation);
        }

        private static void CalculateGradients(out Vector2[] grad)
        {
            grad = new Vector2[256];

            for (var i = 0; i < grad.Length; i++)
            {
                Vector2 gradient;

                do
                {
                    gradient = new Vector2((float)(_random.NextDouble() * 2 - 1), (float)(_random.NextDouble() * 2 - 1));
                }
                while (gradient.LengthSquared() >= 1);

                gradient = gradient / gradient.Length();

                grad[i] = gradient;
            }

        }

        private static float Drop(float t)
        {
            t = Math.Abs(t);
            return 1f - t * t * t * (t * (t * 6 - 15) + 10);
        }

        private static float Q(float u, float v)
        {
            return Drop(u) * Drop(v);
        }

        public static float Noise(float x, float y)
        {
            var cell = new Vector2((float)Math.Floor(x), (float)Math.Floor(y));

            var total = 0f;

            var corners = new[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 0), new Vector2(1, 1) };

            foreach (var n in corners)
            {
                var ij = cell + n;
                var uv = new Vector2(x - ij.X, y - ij.Y);

                var index = _permutation[(int)ij.X % _permutation.Length];
                index = _permutation[(index + (int)ij.Y) % _permutation.Length];

                var grad = _gradients[index % _gradients.Length];

                total += Q(uv.X, uv.Y) * Vector2.Dot(grad, uv);
            }

            return Math.Max(Math.Min(total, 1f), -1f);
        }

        public static void GeneratePerlinNoiseMapTexture(int width, int height, ref RenderTexture noiseTexture, int octaves, bool doReseed = true)
        {
            var noise = GeneratePerlinNoiseMap(width, height, octaves, doReseed);

            if (noiseTexture != null && (noiseTexture.Size.X != width || noiseTexture.Size.Y != height))
            {
                noiseTexture.Dispose();
                noiseTexture = new RenderTexture((uint)width, (uint)height);
            }
            if (noiseTexture == null)
            {
                noiseTexture = new RenderTexture((uint)width, (uint)height);
            }

            var vertexArray = new Vertex[width * height];


            Parallel.For(0, width * height, index =>
            {
                byte col = Convert.ToByte(noise[index] * 255f);
                vertexArray[index].Color = new Color(col, col, col, 255);
                vertexArray[index].Position.X = index % width;
                vertexArray[index].Position.Y = index / width;
            });

            noiseTexture.Clear();
            noiseTexture.Draw(vertexArray, PrimitiveType.Points);
        }

        public static float[] GeneratePerlinNoiseMap(int width, int height, int octaves, bool doReseed = true)
        {
            var data = new float[width * height];

            var min = float.MaxValue;
            var max = float.MinValue;

            if (doReseed)
            {
                Noise2d.Reseed();
            }

            var frequency = 0.5f;
            var amplitude = 1f;
            var persistence = 0.25f;

            for (var octave = 0; octave < octaves; octave++)
            {
                Parallel.For(0
                    , width * height
                    , (offset) =>
                    {
                        var i = offset % width;
                        var j = offset / width;
                        var noise = Noise2d.Noise(i * frequency * 1f / width, j * frequency * 1f / height);
                        noise = data[j * width + i] += noise * amplitude;

                        Interlocked.Exchange(ref min, Math.Min(min, noise));
                        Interlocked.Exchange(ref max, Math.Max(max, noise));
                    }
                );

                frequency *= 2;
                amplitude /= 2;
            }


            Parallel.For(0, height * width, index =>
            {
                data[index] = (data[index] - min) / (max - min);
            });

            return data;
        }

        public static float[,] GenerateNoiseMap(int width, int height, float scale, int octaves, Vector2 position = new Vector2())
        {
            float[,] noiseMap = new float[width, height];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float sampleX = (float)x / width * scale;
                    float sampleY = (float)y / height * scale;

                    float noise = 0f;
                    float frequency = 0f;
                    for (int oct = 1; oct < octaves; oct *= 2)
                    {
                        frequency += 1f / oct;
                        noise += (1f / oct) * Noise(oct * (sampleX + (position.X * scale)), oct * (sampleY + (position.Y * scale)));
                    }

                    noise = noise / frequency;
                    noiseMap[x, y] = noise;
                }
            }

            return noiseMap;
        }
    }
}