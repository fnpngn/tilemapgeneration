﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System.Diagnostics;

namespace CringeFramework.Core
{
    // Define the renderer class that handles the graphics
    public class Renderer : IRenderer
    {
        private RenderWindow window;

        public View View { get; private set; }

        public Renderer(RenderWindow window)
        {
            this.window = window;
            window.SetView(View = new View(new FloatRect(0, 0, window.Size.X, window.Size.Y)));
            window.Resized += OnResize;
        }

        public virtual void Initialize() { }
        public void OnResize(object? sender, SizeEventArgs e)
        {
            var zoom = Application.Instance.ZoomFactor.X;
            var rot = View.Rotation;
            View = new View(new FloatRect(0, 0, e.Width, e.Height));
            View.Zoom(zoom);
            View.Rotation = rot;
            window.SetView(View);
        }

        public void Render(Scene renderScene)
        {
            //if (!window.IsOpen) return;

            window.SetView(View);

            window.Clear(Color.Black);

            window.Draw(renderScene);

            window.Display();
        }

        public void ResizeWindow(uint width, uint height)
        {
            window.Size = new Vector2u(width, height);
        }
        public void ResizeWindow(Vector2u size)
        {
            window.Size = size;
        }

        public void Render(Drawable element)
        {
            throw new NotImplementedException();
        }

        public void ResizeRenderView(uint width, uint height)
        {
            View.Size = new Vector2f(width, height);
        }
    }
}