﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework.Core.Entities
{
    public abstract class Component
    {
        internal Guid Id;
        public bool DoUpdate { get; private set; } = true;
        public abstract void Added();
        public abstract void Removed();
        public abstract void Initialize();
        public abstract void Update();       
    }

    public static class ComponentExtensions
    {
        public static T Clone<T>(this T component) where T : Component
        {
            Console.WriteLine("[DEBUG]: Component Clone CALLED: method incomplete, expect horrible things to happen");
            T clone;
            try
            {
                clone = (T)Activator.CreateInstance(typeof(T), args: component)!;
            } catch (MissingMethodException ex) {
                Console.WriteLine("[DEBUG] Failed to clone Component: Copy Constructor not supported, attempting to do shallow clone");

                clone = (T)Activator.CreateInstance(typeof(T))!;
            }
            
            clone.Id = Guid.NewGuid();
            return clone;
        }
    }
}
