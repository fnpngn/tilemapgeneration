﻿using CringeFramework.Core;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework.Core.Entities
{
    public class Entity
    {
        public string Name;

        internal Guid Id;
        internal Application Application;
        internal List<Component> components;
        private Dictionary<Guid, Component> componentsById;


        private Queue<Component> addComponents;
        private Queue<Component> removeComponents;

        // Parent scene for entity
        public Scene Scene { get;internal set; }

        public Entity() {
            componentsById = new Dictionary<Guid, Component>();
            components = new List<Component>();
            addComponents = new Queue<Component>();
            removeComponents = new Queue<Component>();
        }

        ~Entity()
        {
            if(this is Drawable)
            {
                Scene.RemoveRenderElement((Drawable)this);
            }
        }

        public virtual void Update() { }
        public virtual void Initialize() { }
        public virtual void Added() { }
        public virtual void Removed() { }
        public string GetId()
        {
            return Id.ToString();
        }        
        public void AddComponent<T>(T component) where T : Component
        {
            addComponents.Enqueue(component);
        }

        public void RemoveComponent<T>(T component) where T : Component
        {
            removeComponents.Enqueue(component);
        }

        internal void UpdateInternal()
        {
            ProcessComponentUpdates();
            for(int index = 0; index < components.Count; index++)
            {
                if (!components[index].DoUpdate) continue;
                components[index].Update();
            }

            Update();            
        }
        internal void ProcessComponentUpdates()
        {
            Component component;
            while(removeComponents.Count > 0)
            {
                component = removeComponents.Dequeue();
                components.Remove(component);
                componentsById.Remove(component.Id);
                component.Id = Guid.Empty;
                component.Removed();
            }

            component = null;
            while(addComponents.Count > 0)
            {
                component = addComponents.Dequeue();
                component.Id = Guid.NewGuid();
                componentsById.Add(component.Id, component);
                components.Add(component);
                component.Added();
            }
        }

    }
    public static class EntityExtensions
    {
        public static T Clone<T>(this T entity) where T : Entity
        {
            Console.WriteLine("[DEBUG]: Entity Clone CALLED: method incomplete, expect horrible things to happen");
            var clone = new Entity();
            clone.Id = Guid.NewGuid();
            clone.Name = entity.Name;
            foreach (var component in entity.components)
            {
                clone.AddComponent(component.Clone());
            }
            entity.Scene.Add((T)clone);
            //clone.Initialize();
            return (T)clone;
        }
    }
}
