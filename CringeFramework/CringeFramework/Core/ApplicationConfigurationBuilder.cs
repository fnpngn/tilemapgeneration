﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CringeFramework.Core.Entities;
using SFML.Graphics;

namespace CringeFramework.Core
{
    public class ApplicationConfiguration
    {
        public string WindowName { get; set; }
        public Component[] Components { get; set; }
        public Dictionary<string, Scene> Scenes { get; set; }
        public InputManager InputManager { get; set; }
        public Image Icon { get; set; }
        public bool RunWithoutFocus { get; set; }
    }

    public class ApplicationConfigurationBuilder
    {
        private ApplicationConfiguration _config;
        private List<Component> components;
        private string iconPath;
        private Image applicationIcon;

        public ApplicationConfigurationBuilder()
        {
            _config = new ApplicationConfiguration()
            {
                Components = Array.Empty<Component>(),
                WindowName = "Application",
                Scenes = new Dictionary<string, Scene>() { },
            };
            components = new List<Component>();
        }

        public ApplicationConfigurationBuilder WithWindowName(string name)
        {
            _config.WindowName = name;
            return this;
        }

        public ApplicationConfigurationBuilder WithComponent(Component component)
        {
            components.Add(component);
            return this;
        }

        public ApplicationConfigurationBuilder WithScene<T>(string id) where T : Scene
        {
            var scene = Activator.CreateInstance(typeof(T), args: id)!;
            return WithScene((Scene)scene, id);
        }

        public ApplicationConfigurationBuilder WithScene(Scene scene, string? id = null)
        {
            var key = id ?? scene.Name;
            if (_config.Scenes.ContainsKey(key))
            {
                throw new ArgumentException("Failed to add application scene: identifier \"" + id + "\" is already taken");
            }

            _config.Scenes[key] = scene;
            return this;
        }

        public ApplicationConfigurationBuilder WithInputManager(InputManager inputManager)
        {
            _config.InputManager = inputManager;
            return this;
        }

        public ApplicationConfigurationBuilder WithIcon(string filePath)
        {
            if (applicationIcon is not null || iconPath is not null)
            {
                throw new Exception("Attempted to add app icon while one is alredy registered");
            }

            iconPath = filePath;
            return this;
        }

        public ApplicationConfigurationBuilder WithIcon(Image icon)
        {
            if (applicationIcon is not null || iconPath is not null)
            {
                throw new Exception("Attempted to add app icon while one is already registered");
            }

            applicationIcon = icon;
            return this;
        }

        public ApplicationConfiguration Build()
        {
            _config.Components = components.ToArray();
            if (_config.InputManager == null)
            {
                _config.InputManager = new InputManager();
            }

            if (iconPath is not null || applicationIcon is not null)
            {
                _config.Icon = RetrieveApplicationIcon();
            }

            return _config;
        }


        private Image RetrieveApplicationIcon()
        {
            Image icon;
            if (iconPath is not null)
            {
                icon = new Image(iconPath);
            }
            else if (applicationIcon is not null)
            {
                icon = applicationIcon;
            }
            else throw new ArgumentNullException("Failed to load application icon: no resource to parse available");

            if (icon.Size.X != 32 || icon.Size.Y != 32)
            {
                icon = icon.ResizeEager(32u, 32u);
            }

            return icon;
        }

    }
}
