﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CringeFramework.Core.Coroutine
{
    public abstract class Coroutine<T> : IEnumerator<T>
    {
        public T Current { get; protected set; }

        object IEnumerator.Current => Current;

        public abstract void Dispose();

        public abstract bool MoveNext();

        public abstract void Reset();
    }

    public abstract class Coroutine : Coroutine<object>
    {
        public Coroutine()
        {
            Current = MoveNext();
        }
        public override void Dispose() { }

        public override bool MoveNext() { return true; }

        public override void Reset() { }
    }
}
