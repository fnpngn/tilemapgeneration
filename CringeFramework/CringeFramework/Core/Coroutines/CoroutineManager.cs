﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Window;
using System.Diagnostics;

namespace CringeFramework.Core.Coroutine
{
    public class CoroutineManager
    {
        public int Count => coroutineList.Count;
        private int index;
        private Dictionary<int, IEnumerator> coroutines;
        private List<IEnumerator> coroutineList;
        internal Application Application { private get; set; }
        public CoroutineManager()
        {
            index = 0;
            coroutines = new Dictionary<int, IEnumerator>();
            coroutineList = new List<IEnumerator>();
        }

        public int Start(IEnumerator coroutine)
        {
            coroutineList.Add(coroutine);
            coroutines[index] = coroutine;
            return index++;
        }

        public void Stop(int id)
        {
            if (coroutines.ContainsKey(id))
            {
                coroutineList.Remove(coroutines[id]);
                coroutines.Remove(id);
            }
        }

        private void Stop(IEnumerator coroutine)
        {
            coroutineList.Remove(coroutine);
            coroutines.Remove(coroutines.FirstOrDefault(x => x.Value == coroutine).Key);
        }

        private bool Next(IEnumerator coroutine)
        {
            //Nested coroutine handling for custom behaviors (frame skip etc.)
            if (coroutine.Current is IEnumerator && Next((IEnumerator)coroutine.Current))
            {
                return true;
            }
            return coroutine.MoveNext();
        }

        public void Update()
        {
            for (int i = 0; i < coroutineList.Count; i++)
            {
                if (coroutineList[i].Current is IEnumerator)
                    if (Next((IEnumerator)coroutineList[i].Current))
                        continue;
                if (!coroutineList[i].MoveNext())
                {
                    coroutines.Remove(coroutines.FirstOrDefault(c => c.Value == coroutineList[i]).Key);
                    coroutineList.RemoveAt(i--);
                }
            }
        }

        public IEnumerator WaitForSeconds(float seconds)
        {
            if (Application is null)
            {
                var watch = Stopwatch.StartNew();
                while (watch.ElapsedMilliseconds < seconds * 1000f)
                {
                    yield return 0;
                }
                watch.Stop();
                yield break;
            }


            float elapsed = 0;
            while (elapsed < seconds)
            {
                elapsed += Application.DeltaTime * 0.001f;
                yield return 0;
            }
        }

        public IEnumerator WaitForFrames(int frames)
        {
            int elapsed = 0;
            while (elapsed++ < frames)
            {
                yield return 0;
            }
        }
    }
}
