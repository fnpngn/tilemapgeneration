﻿using CringeFramework.Core.Entities;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CringeFramework.Core
{
    public abstract class Scene : Drawable
    {
        public string Name { get; internal set; }
        private List<Drawable> drawables;
        private List<Entity> entities;

        private Dictionary<Guid, Entity> entitiesById;

        private Queue<Entity> addEntities;
        private Queue<Entity> removeEntities;

        public Application Application { get; internal set; }
        public Scene(string name)
        {
            Name = name;
            drawables = new List<Drawable>(10);
            entities = new List<Entity>();
            entitiesById = new Dictionary<Guid, Entity>();
            addEntities = new Queue<Entity>();
            removeEntities = new Queue<Entity>();
        }

        public virtual void Update() { }
        public virtual void Initialize() { }
        public virtual void Unload() { }

        public void Add<T>(T entity) where T : Entity
        {
            if (entity.Scene is not null)
            {
                throw new Exception("Attempted to add entity which is already another scene. Try using Clone or creating a different entity");
            }
            addEntities.Enqueue(entity);
        }

        public void Remove<T>(T entity) where T : Entity
        {
            if (entity.Scene != this)
            {
                throw new Exception($"Attempted to remove entity {entity.Name} which is in another scene");
            }

            removeEntities.Enqueue(entity);
        }
        public void AddRenderElement<T>(T element) where T : Drawable
        {
            //Console.WriteLine("Adding element to scene: " + Name);
            drawables.Add(element);
        }
        public void AddRenderElements<T>(T[] elements) where T : Drawable
        {
            drawables.AddRange(elements.Cast<Drawable>());
        }

        public void RemoveRenderElement<T>(T element) where T : Drawable
        {
            drawables.Remove(element);
        }

        public void RemoveRenderElements<T>(T[] elements) where T : Drawable
        {
            for (int i = 0; i < elements.Length; i++)
            {
                drawables.Remove(elements[i]);
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            //Console.WriteLine("Drawing scene: " + Name);
            for (int i = 0; i < drawables.Count; i++)
            {
                target.Draw(drawables[i], states);
            }
        }

        internal void UpdateInternal()
        {
            ProcessEntityUpdates();
            Update();
            for(int i = 0; i<entities.Count; i++)
            {
                entities[i].UpdateInternal();
            }
        }

        internal void InitializeInternal()
        {
            Initialize();
            for (int i = 0; i < entities.Count; i++)
            {
                entities[i].UpdateInternal();
            }
        }

        internal void UnloadInternal()
        {
            Unload();
        }

        private void ProcessEntityUpdates()
        {
            Entity entity;
            while (removeEntities.Count > 0)
            {
                entity = removeEntities.Dequeue();
                entities.Remove(entity);
                entitiesById.Remove(entity.Id);
                entity.Id = Guid.Empty;
                entity.Scene = null;
                if (entity is Drawable)
                {
                    this.RemoveRenderElement((Drawable)entity);
                }
                entity.Removed();
            }
            entity = null;
            while (addEntities.Count > 0)
            {
                entity = addEntities.Dequeue();
                entity.Id = Guid.NewGuid();
                entity.Scene = this;
                entities.Add(entity);
                entitiesById[entity.Id] = entity;
                if (entity is Drawable)
                {
                    this.AddRenderElement((Drawable)entity);
                }
                entity.Added();
                entity.Initialize();
            }
        }
    }
}
