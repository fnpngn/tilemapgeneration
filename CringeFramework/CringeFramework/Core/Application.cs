﻿using CringeFramework.Core.Coroutine;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Component = CringeFramework.Core.Entities.Component;

namespace CringeFramework.Core

{
    public class Application : IDisposable
    {
        public static Application Instance { get { return lazy.Value; } }
        private static readonly Lazy<Application> lazy = new Lazy<Application>(() => new Application());
        private static Mutex mux;

        private RenderWindow window;

        private Renderer renderer;
        private InputManager inputManager;
        private Component[] components;
        private Dictionary<string, Scene> scenes;
        private CoroutineManager coroutineManager;

        private Process process = Process.GetCurrentProcess();

        private bool isInitialized;

        private Stopwatch renderTime;
        private Stopwatch updateTime;
        private Stopwatch uptime;
        private float deltaTime = 0;
        private float frameTime;
        private float lastTime = 0;
        private float skipTime;
        private bool hasFocus;
        private float targetFPS = 60f;
        private string title;
        ///<summary> 
        /// Keep updating in a window that has lost focus
        ///</summary>
        public bool IndefiniteUpdate;

        private Image icon;

        public float Timer;

        public Action OnInit = delegate { };
        public Action OnUpdate = delegate { };
        public Action OnFocusLost = delegate { };
        public Action OnFocusGained = delegate { };
        public Color BackgroundColor = Color.Black;



        public string Title { get { return title; } private set { title = value; window.SetTitle(value); } }
        public ApplicationState State { get; private set; }
        internal RenderWindow Window => window;
        public Scene ActiveScene { get; private set; }
        public View View => renderer.View;
        public CoroutineManager Coroutines => coroutineManager;
        public InputManager InputManager => inputManager;
        public Vector2f ZoomFactor => new Vector2f(View.Size.X / window.Size.X, View.Size.Y / window.Size.Y);
        public bool HasFocus => hasFocus;
        public float DeltaTime => deltaTime;

        private Application()
        {
            isInitialized = false;
            mux = new Mutex();
            renderTime = new Stopwatch();
            updateTime = new Stopwatch();
            uptime = new Stopwatch();
        }

        public Application Initialize(ApplicationConfiguration config)
        {
            if (isInitialized) throw new InvalidOperationException("Application Initialization cannot be called: Application already initialized");

            State = ApplicationState.Initializing;
            uptime.Start();

            title = config.WindowName;
            window = new RenderWindow(VideoMode.DesktopMode, Title);
            window.SetIcon(32, 32, config.Icon.Pixels);
            window.GainedFocus += OnGainedFocus;
            window.LostFocus += OnLostFocus;

            components = config.Components;
            scenes = config.Scenes;

            inputManager = config.InputManager;

            coroutineManager = new CoroutineManager();

            renderer = new Renderer(window);

            window.SetFramerateLimit(Convert.ToUInt16(targetFPS));

            window.Closed += OnWindowClosed;

            window.Display();

            window.SetActive(true);

            inputManager.InitializeInternal(window);

            for (int i = 0; i < components.Length; i++)
            {
                components[i].Initialize();
            }

            if (scenes.Count > 0)
            {
                foreach (var (_, scene) in scenes)
                {
                    scene.Application = this;
                    scene.InitializeInternal();
                }
                ActiveScene = scenes.First().Value;
            }

            OnInit.Invoke();

            hasFocus = window.HasFocus();
            isInitialized = true;
            return this;
        }
        public void Run()
        {
            if (!isInitialized)
            {
                throw new InvalidOperationException("Failed to start execution: Application not initialized");
            }

            if (!UpdateApplicationState(ApplicationState.Initializing, ApplicationState.Running))
            {
                throw new ApplicationException("Invalid Application State");
            }

            if (ActiveScene is null)
            {
                throw new ArgumentNullException("Failed to run application: ActiveScene was not set");
            }

            lastTime = uptime.ElapsedMilliseconds;
            while (State == ApplicationState.Running)
            {
                frameTime = 1000f / targetFPS;
                skipTime = frameTime * 2f;
                // would pause state be any better?
                if (!hasFocus && !IndefiniteUpdate)
                {
                    window.DispatchEvents();
                    deltaTime = 0f;
                    continue;
                }

                deltaTime = uptime.ElapsedMilliseconds - lastTime;
                lastTime = uptime.ElapsedMilliseconds;
                
                updateTime.Restart();
                Update();
                OnUpdate.Invoke();
                updateTime.Stop();

                renderTime.Restart();
                renderer.Render(ActiveScene);
                renderTime.Stop();
            }

            ShutdownInternal();
        }
        public void Shutdown()
        {
            UpdateApplicationState(ApplicationState.Running, ApplicationState.Halt);
        }

        public void SetActiveScene(string name)
        {
            if (!scenes.ContainsKey(name))
            {
                throw new ArgumentException("Invalid scene name provided when trying to set scene as active: " + name);
            }
            ActiveScene = scenes[name];
        }

        private void Update()
        {
            inputManager.UpdateInternal();
            ActiveScene.UpdateInternal();
            for (int i = 0; i < components.Length; i++)
            {
                components[i].Update();
            }
            coroutineManager.Update();
        }

        private void ShutdownInternal()
        {
            Console.WriteLine("Shutting down");
            if (window.IsOpen) window.Close();

            ActiveScene?.UnloadInternal();

            UpdateApplicationState(ApplicationState.Halt, ApplicationState.Shutdown);
        }

        private void OnWindowClosed(object? sender, EventArgs e)
        {
            window.Close();
            UpdateApplicationState(ApplicationState.Running, ApplicationState.Halt);
        }

        private void OnLostFocus(object? sender, EventArgs e)
        {
            hasFocus = false;
            this.OnFocusLost();
        }

        private void OnGainedFocus(object? sender, EventArgs e)
        {
            hasFocus = true;
            this.OnFocusGained();
        }

        private bool UpdateApplicationState(ApplicationState old, ApplicationState newState)
        {
            mux.WaitOne();
            if (State != old)
            {
                mux.ReleaseMutex();
                return false;
            }

            State = newState;
            mux.ReleaseMutex();
            return true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing) window.Dispose();
        }

        public enum ApplicationState
        {
            Initializing,
            Running,
            Halt,
            Shutdown
        }
    }

}
